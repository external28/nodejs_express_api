const { db } = require('./db_config')

const getUsers = (request, response) => {
const query = 'SELECT * FROM users'
    db.query(query, (error, results) => {
        if (error) {
            response.status(500).json(error)
	    console.log("Error:", error );
        }
	    else response.status(200).json(results.rows)
    })
}

const verifyCredentials = (email,password) => {
    const values = [email, password]
    const queryPromise = new Promise((resolve, reject) => {
        const query = 'SELECT * from users WHERE email=$1 AND password=$2'
        db.query(query, values, (error, results) => {
            if (error) {
		console.log('error occured: ' + error);
		return resolve(false); //check in the future
	    }
	    if (results.rows.length == 1) {
                resolve(true)
            }
            resolve(false)
        })
    })
    return queryPromise
}

const login = (request, response) => {
    // user login informations
    const user = {
        email: request.body.email,
        password: request.body.password
    }    

    verifyCredentials(user.email, user.password)
    .then( (connected) => {
        if (connected) {
        // connection is validate, send a 200 status code by default
            return response.json({
                message: 'connected',
            })
        }
        // else connection refused, we send an authentification error
        return response.status(401).json({
            message: 'unauthorized',
        })
    })
}

module.exports = {
	getUsers,
	login
}
