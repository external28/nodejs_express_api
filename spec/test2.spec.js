var request = require('request')
//var baseUrl = 'http://localhost:8080'// authentification routes
var baseUrl = 'http://vm-hyper:8080'// authentification routes
describe('login ', function () {
    it('login function should log the user in and return 1 argument msg connected  and 2 cookies and status code 200', async function (done) {
        var User = {
            email: 'user.un@test.com',
            password: 'user1password',
        }
        options = {
            method: 'POST',
            url: baseUrl + '/login',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(User)
        }
        request(options, function (error, response) {
           if (error) throw new Error(error)
           const responseLength =   Object.keys(JSON.parse(response.body)).length
           expect(responseLength).toBe(1)
           expect(JSON.parse(response.body).message)
           .toEqual('connected')
            expect(response.statusCode).toBe(200)
            done()
        })
    })    

    it('login with a bad password, should fail, return a 401 status code and 1 argument msg unauthorized', async function (done) {        
	User = {
            email: 'user@dixhuit.test',
            password: 'wrongpassword'
        }
        options = {
            method: 'POST',
            url: baseUrl + '/login',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(User)
        }
        request(options, function (error, response) {
            if (error) throw new Error(error)
            const responseLength =  Object.keys(JSON.parse(response.body)).length
            expect(responseLength).toBe(1)
            expect(JSON.parse(response.body).message)
            .toEqual('unauthorized')
            expect(response.statusCode).toBe(401)
            done()
        })
    })
})

describe('TestDb ', function () {it('how many users have I in the    database ? should response with a length of 3', (done) => {
        expect(1).toEqual(1)
        done()
    })
})
