FROM node:12.19.0
WORKDIR /app
COPY . /app
RUN npm i -g npm@latest && npm install
EXPOSE 8080
CMD [ "node", "index.js" ]
