builded-test:
	sudo docker build -t myimage .
	sudo docker-compose up -d
	sleep 5
	sudo docker exec postgres psql -U postgres -h postgres -f home/dumps/tests/medium.sql
	sudo docker exec myimage npx jasmine spec/api.spec.js spec/*.js
	sudo docker-compose down
