// config
// PostgreSQL config
const configPG = {
user: 'postgres',
//host: 'localhost',
host: 'vm-hyper',
database: 'medium',
password: 'postgres',
port: '5432'
}

const Pool = require('pg').Pool
const db = new Pool(configPG)
module.exports = { db }
