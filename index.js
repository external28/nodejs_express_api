// import express (after npm install express)
const express = require('express');
const queries = require('./queries');
const bodyParser = require('body-parser');
// create new express app and save it as “app”
const app = express();
app.use(bodyParser.json({ limit: '50mb' }));
// server configuration
const PORT = 8080;
// create a route for the app
app.get('/', (req, res) => {
	res.send('Hello World');
});

//list DB users
app.get('/users/', queries.getUsers);
app.post('/login', queries.login);

// make the server listen to requests
app.listen(PORT, () => {console.log(`Server running at: http://localhost:${PORT}/`);});
